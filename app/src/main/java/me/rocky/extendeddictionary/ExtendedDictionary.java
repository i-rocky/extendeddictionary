package me.rocky.extendeddictionary;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ExtendedDictionary extends AppCompatActivity implements TextToSpeech.OnInitListener {
    static Handler handler;
    TextToSpeech TTS;
    ArrayAdapter<String> arrayAdapter;
    List<String> arrayList;
    boolean bengali;
    boolean busyLki;
    boolean busyLku;
    public static Database db;
    DatabaseHelper dbh;
    boolean english;
    AsyncTask lki;
    AsyncTask lku;
    AdView mAdView;
    SharedPreferences mDB;
    EditText query;
    ListView resultsList;

    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ProgressDialog dlg = ProgressDialog.show(this, null, "Please Wait...");
        sAnalytics = GoogleAnalytics.getInstance(this);
        handler = new Handler(Looper.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == 0) {
                    dlg.dismiss();
                    ExtendedDictionary.this.setContentView(R.layout.activity_extended_dictionary);
                    ExtendedDictionary.this.initialize();
                }
            }
        };
        new Thread(new Runnable() {
            @Override
            public void run() {
                ExtendedDictionary.this.loadDB();
                ExtendedDictionary.handler.sendEmptyMessage(0);
            }
        }).start();
    }

    synchronized public Tracker getDefaultTracker() {
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(R.xml.global_tracker);
        }
        return sTracker;
    }

    @Override
    public void onInit(int status) {
        if (status == 0) {
            this.TTS.setLanguage(Locale.UK);
        } else {
            Toast.makeText(this, "Text To Speech not available.", Toast.LENGTH_LONG).show();
        }
    }


    private class lookItUp extends AsyncTask<String, Void, ArrayList<String>> {
        private lookItUp() {
        }

        protected void onPreExecute() {
            ExtendedDictionary.this.busyLku = true;
        }

        protected ArrayList<String> doInBackground(String... word) {
            getDefaultTracker()
                    .send(new HitBuilders.EventBuilder()
                            .setLabel(word[0])
                            .setAction("LookUp")
                            .build());
            ArrayList<String> arr = db.getMatches(word[0], ExtendedDictionary.this.dbh, ExtendedDictionary.this.bengali, ExtendedDictionary.this.english);
            if (arr.size() < 1) {
                cancel(true);
            }
            return arr;
        }

        protected void onProgressUpdate(Void... values) {
        }

        protected void onPostExecute(ArrayList<String> result) {
            ExtendedDictionary.this.arrayAdapter.clear();
            ExtendedDictionary.this.arrayAdapter.setNotifyOnChange(false);
            for (Object aResult : result) {
                ExtendedDictionary.this.arrayAdapter.add((String) aResult);
            }
            ExtendedDictionary.this.arrayAdapter.notifyDataSetChanged();
            ExtendedDictionary.this.busyLku = false;
            ExtendedDictionary.this.resultsList.setSelectionAfterHeaderView();
        }
    }

    private class lookUpInfo extends AsyncTask<String, Void, String> {
        String baseWord;
        private lookUpInfo() {
        }

        protected void onPreExecute() {
            ExtendedDictionary.this.busyLki = true;
        }

        protected String doInBackground(String... word) {
            this.baseWord = word[0];
            return db.getInfo(word[0], ExtendedDictionary.this.dbh, ExtendedDictionary.this.bengali, ExtendedDictionary.this.english);
        }

        protected void onProgressUpdate(Void... values) {
        }

        protected void onPostExecute(String html) {
            new AlertDialog.Builder(ExtendedDictionary.this)
                    .setTitle(db.capitalize(this.baseWord))
                    .setMessage(Html.fromHtml(html))
                    .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ExtendedDictionary.this.busyLki = false;
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .show();
        }
    }

    void loadDB() {
        this.dbh = new DatabaseHelper(this);
        try {
            this.dbh.createDB();
        } catch (IOException e) {
        }
        try {
            this.dbh.openDB();
        } catch (SQLException e2) {
        }
    }

    void initialize() {
        getDefaultTracker()
                .send(new HitBuilders.EventBuilder()
                        .setLabel("HomeScreen")
                        .build());

        this.mAdView = (AdView) findViewById(R.id.ad_view);
        this.mAdView.loadAd(new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build());
        this.mDB = getPreferences(0);
        this.bengali = this.mDB.getBoolean("bengali", true);
        this.english = this.mDB.getBoolean("english", true);
        db = new Database();
        this.TTS = new TextToSpeech(this, this);
        this.bengali = true;
        this.english = true;
        this.resultsList = (ListView) findViewById(R.id.resultList);
        this.resultsList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {}
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (totalItemCount == (firstVisibleItem + visibleItemCount)) {
                    ExtendedDictionary.this.mAdView.setVisibility(View.INVISIBLE);
                } else {
                    ExtendedDictionary.this.mAdView.setVisibility(View.VISIBLE);
                }
            }
        });
        this.resultsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String word = ((TextView) view).getText().toString().toLowerCase();
                if (ExtendedDictionary.this.busyLki) {
                    ExtendedDictionary.this.lki.cancel(true);
                }

                Intent intent = new Intent(getBaseContext(), WordDetails.class);
                intent.putExtra("word", word);
                startActivity(intent);

                //ExtendedDictionary.this.lki = new lookUpInfo().execute(new String[]{word});
            }
        });
        this.resultsList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                ExtendedDictionary.this.speak(((TextView) view).getText().toString().toLowerCase());
                return true;
            }
        });
        this.arrayList = new ArrayList<>();
        this.arrayList.add("Rocky");
        this.arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, this.arrayList);
        this.resultsList.setAdapter(this.arrayAdapter);
        this.query = (EditText) findViewById(R.id.query);
        this.query.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                ExtendedDictionary.this.lookUp(ExtendedDictionary.this.query.getText().toString().trim().toLowerCase());
            }
        });
        this.busyLki = false;
        this.busyLku = false;
        lookUp(BuildConfig.FLAVOR);
    }

    void speak(String word) {
        if (Build.VERSION.RELEASE.startsWith("5")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                this.TTS.speak(word, 0, null, null);
            }
        } else {
            this.TTS.speak(word, 0, null);
        }
    }

    void lookUp(String word) {
        if (word.equals(BuildConfig.FLAVOR)) {
            word = "a";
        }
        if (this.busyLku) {
            this.lku.cancel(true);
        }
        this.lku = new lookItUp().execute(new String[]{word});
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            final CheckBox engCheckBox = new CheckBox(this);
            engCheckBox.setText("English");
            engCheckBox.setChecked(this.english);
            final CheckBox bangCheckBox = new CheckBox(this);
            bangCheckBox.setText("Bengali");
            bangCheckBox.setChecked(this.bengali);
            LinearLayout layout = new LinearLayout(this);
            layout.addView(engCheckBox);
            layout.addView(bangCheckBox);
            new AlertDialog.Builder(this)
                    .setTitle("Settings")
                    .setView(layout)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            boolean nBan = bangCheckBox.isChecked();
                            boolean nEng = engCheckBox.isChecked();
                            if (nBan || nEng) {
                                SharedPreferences.Editor spe = ExtendedDictionary.this.mDB.edit();
                                spe.putBoolean("bengali", nBan);
                                spe.putBoolean("english", nEng);
                                spe.apply();
                                ExtendedDictionary.this.bengali = nBan;
                                ExtendedDictionary.this.english = nEng;
                                return;
                            }
                            Toast.makeText(ExtendedDictionary.this, "What you want this app to use for? Nothing changed.", Toast.LENGTH_LONG).show();
                        }
                    })
                    .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .show();
            return true;
        } else if (id == R.id.action_share) {
            getDefaultTracker()
                    .send(new HitBuilders.EventBuilder()
                            .setLabel("Sharing App")
                            .build());
            String filePath = getApplicationContext().getApplicationInfo().sourceDir;
            Intent shareIntent = new Intent("android.intent.action.SEND");
            shareIntent.setType("*/*");
            shareIntent.putExtra("android.intent.extra.STREAM", Uri.fromFile(new File(filePath)));
            startActivity(Intent.createChooser(shareIntent, "Share Via"));
            return true;
        } else if (id == R.id.action_help) {
            new AlertDialog.Builder(this)
                    .setTitle("Help")
                    .setMessage(Html.fromHtml("<p><i>1.</i> Type a word to get a list of matching words.<br/></p><p><i>2.</i> Click on the word to view Bengali and English definition for that word.<br/></p><p><i>3.</i> Long press word to make your device speak it (Only works if Text To Speech available).<br/></p><p><i>4.</i> You can turn Bengali or English definition off from Settings.</p>"))
                    .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .show();
            return true;
        } else if (id != R.id.action_about) {
            return super.onOptionsItemSelected(item);
        } else {
            new AlertDialog.Builder(this)
                    .setTitle("MD. Rocky PK.")
                    .setView(LayoutInflater.from(this)
                            .inflate(R.layout.about, null))
                    .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .show();
            return true;
        }
    }

    @Override
    public void onPause() {
        if (this.mAdView != null) {
            this.mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (this.mAdView != null) {
            this.mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (this.mAdView != null) {
            this.mAdView.destroy();
        }
        if(this.TTS != null) {
            this.TTS.stop();
            this.TTS.shutdown();
        }
        if(db!=null)
            dbh.close();
        super.onDestroy();
    }
}
