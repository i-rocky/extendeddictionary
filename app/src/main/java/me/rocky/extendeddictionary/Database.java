package me.rocky.extendeddictionary;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

class Database {
    ArrayList<String> getMatches(String word, DatabaseHelper db, boolean bengali, boolean english) {
        ArrayList<String> matches = new ArrayList<>();
        SQLiteDatabase sdb = db.getReadableDatabase();
        Cursor c = null;
        if (bengali) {
            try {
                c = sdb.rawQuery("SELECT `word` FROM `bengali` WHERE `word` LIKE \"" + word + "%\" LIMIT 100", null);
                if (c.moveToFirst()) {
                    do {
                        matches.add(c.getString(c.getColumnIndex("word")).trim());
                    } while (c.moveToNext());
                }
            } catch (Exception e) {
                Log.e("getMatches()", e.getMessage());
            }
        }
        if (english) {
            c = null;
            try {
                c = sdb.rawQuery("SELECT `word` FROM `definition` WHERE `word` LIKE \"" + capitalize(word) + "%\" LIMIT 100", null);
                if (c.moveToFirst()) {
                    do {
                        matches.add(c.getString(c.getColumnIndex("word")).trim());
                    } while (c.moveToNext());
                }
            } catch (Exception e) {
                Log.e("second getMatches()", e.getMessage());
            }
        }
        if (c != null) {
            c.close();
        }
        if (matches.isEmpty()) {
            matches.add("Not found");
        }
        ArrayList<String> matchesToReturn = new ArrayList<>();
        for (Object match : matches) {
            String each = capitalize((String) match);
            if (!matchesToReturn.contains(each)) {
                matchesToReturn.add(each);
            }
            if (matchesToReturn.size() > 49) {
                break;
            }
        }
        sdb.close();
        return matchesToReturn;
    }

    String capitalize(String line) {
        return Character.toUpperCase(line.charAt(0)) + line.substring(1);
    }

    String getInfo(String word, DatabaseHelper db, boolean bengali, boolean english) {
        SQLiteDatabase sdb = db.getReadableDatabase();
        Cursor c = null;
        ArrayList<String> added = new ArrayList<>();
        String html = BuildConfig.FLAVOR;
        int i = 1;
        if (bengali) {
            try {
                c = sdb.rawQuery("SELECT * FROM `bengali` WHERE `word` LIKE \"" + word + "\"", null);
                if (c.moveToFirst()) {
                    html = html + "<b><font color='black'>Bengali Dictionary</font></b><br/>";
                    do {
                        String bengal = c.getString(c.getColumnIndex("bengali"));
                        if (!added.contains(bengal)) {
                            added.add(bengal);
                            html = html + "<p><font color='black'>\t" + i + ". " + bengal + "</font></p>";
                            i++;
                        }
                    } while (c.moveToNext());
                    html = html + "<br/><br/>";
                }
            } catch (Exception e) {
                Log.e("getInfo()", e.getMessage());
            }
        }
        if (english) {
            i = 1;
            c = null;
            try {
                c = sdb.rawQuery("SELECT * FROM `definition` WHERE `word` LIKE \"" + word + "\"", null);
                if (c.moveToFirst()) {
                    html = html + "<b><font color='black'>Definition Dictionary</font></b><br/>";
                    do {
                        String type = c.getString(c.getColumnIndex("type"));
                        String definition = c.getString(c.getColumnIndex("definition"));
                        if (!added.contains(definition)) {
                            added.add(definition);
                            html = html + "<p><font color='black'>\t" + i + ". [</font><font color='blue'><i>" + type + "</i></font><font color='black'>] - " + definition + "</font></p>";
                            i++;
                        }
                    } while (c.moveToNext());
                }
            } catch (Exception e) {
                Log.e("getInfo()2", e.getMessage());
            }
        }
        if (c != null) {
            c.close();
        }
        sdb.close();
        return html;
    }
}
