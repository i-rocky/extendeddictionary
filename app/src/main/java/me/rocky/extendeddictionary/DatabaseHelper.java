package me.rocky.extendeddictionary;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

class DatabaseHelper extends SQLiteOpenHelper {
    private static String DB_NAME;
    private static String DB_PATH;
    private Context context;
    private SQLiteDatabase dbObj;

    static {
        DB_PATH = "data/data/me.rocky.extendeddictionary/databases/";
        DB_NAME = "database";
    }

    DatabaseHelper(Context context) {
        super(context, DB_NAME, null, 3);
        this.context = context;
    }

    void createDB() throws IOException {
        getReadableDatabase();
        try {
            copyDB();
        } catch (IOException e) {
            Log.e("createDB()", e.getMessage());
        }
    }

    private void copyDB() throws IOException {
        try {
            InputStream ip = this.context.getAssets().open(DB_NAME + ".db");
            OutputStream output = new FileOutputStream(DB_PATH + DB_NAME);
            byte[] buffer = new byte[3096];
            while (true) {
                int length = ip.read(buffer);
                if (length > 0) {
                    output.write(buffer, 0, length);
                } else {
                    output.flush();
                    output.close();
                    ip.close();
                    return;
                }
            }
        } catch (IOException e) {
            Log.e("copyDB()", e.getMessage());
        }
    }

    void openDB() throws SQLException {
        this.dbObj = SQLiteDatabase.openDatabase(DB_PATH + DB_NAME, null, 0);
    }

    public synchronized void close() {
        if (this.dbObj != null) {
            this.dbObj.close();
        }
        super.close();
    }

    public void onCreate(SQLiteDatabase db) {
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        this.onCreate(db);
    }
}
